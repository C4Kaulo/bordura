package inventory.repository;

import inventory.model.OutsourcedPart;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

// S + R
class InventoryRepositoryIntegrationTest {
    InventoryRepository repository;
    InventoryService service;

    @BeforeEach
    void setUp() {
        repository = new InventoryRepository();
        service = new InventoryService(repository);
    }

    @Test
    void addOutsourcePart() {
        OutsourcedPart part1 = mock(OutsourcedPart.class);
        OutsourcedPart part2 = mock(OutsourcedPart.class);

        service.addOutsourcePart(part1);
        service.addOutsourcePart(part2);

        assertEquals(part1, service.getAllPart().get(0));
        assertEquals(part2, service.getAllPart().get(1));
    }

    @Test
    void getAllPart() {
        OutsourcedPart part1 = mock(OutsourcedPart.class);
        OutsourcedPart part2 = mock(OutsourcedPart.class);

        service.addOutsourcePart(part1);
        service.addOutsourcePart(part2);

        assertEquals(2, service.getAllPart().size());
    }
}