package inventory.repository;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

class InventoryRepositoryUnitTest {
    private InventoryRepository repository;

    @BeforeEach
    void setUp() {
        repository = new InventoryRepository();
        assertEquals(0, repository.getInventory().getAllParts().size());
    }

    @Test
    void getAllParts() {
        repository.addPart(mock(Part.class));
        repository.addPart(mock(Part.class));
        repository.addPart(mock(Part.class));
        assertEquals(3, repository.getInventory().getAllParts().size());
    }

    @Test
    void addPat() {
        Part p1 = mock(Part.class);
        Mockito.when(p1.getPartId()).thenReturn(1);

        repository.addPart(p1);

        assertEquals(1, repository.getInventory().getAllParts().get(0).getPartId());
    }
}