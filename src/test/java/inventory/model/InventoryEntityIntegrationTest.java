package inventory.model;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class InventoryEntityIntegrationTest {
    InventoryService service;
    InventoryRepository repository;

    @BeforeEach
    private void setUp() {
        repository = new InventoryRepository();
        service = new InventoryService(repository);
    }

    @Test
    void getAllParts() {
        OutsourcedPart p1 = new OutsourcedPart(1, "", 0.0f, 0,0,0,"");
        OutsourcedPart p2 = new OutsourcedPart(1, "", 0.0f, 0,0,0,"");
        OutsourcedPart p3 = new OutsourcedPart(1, "", 0.0f, 0,0,0,"");

        repository.addPart(p1);
        repository.addPart(p2);
        repository.addPart(p3);

        assertEquals(3, service.getAllPart().size());
    }

    @Test
    void addPart() {
        OutsourcedPart p1 = new OutsourcedPart(1, "", 0.0f, 0,0,0,"");

        repository.addPart(p1);

        assertEquals(1, service.getAllPart().get(0).getPartId());
    }
}