package inventory.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

// Entitate
class InventoryEntitiyUnitTest {
    int validId = 1;
    String validName = "validName";
    double validPrice = 1.0f;
    int validInStock = 2;
    int validMin = 1;
    int validMax = 10;
    String validCompanyName = "validName";

    @Test
    void testGetter() {
        OutsourcedPart part = null;

        part = new OutsourcedPart(validId, validName, validPrice, validInStock, validMin, validMax, validCompanyName);

        assertEquals(part.getPartId(), validId);
        assertEquals(part.getName(), validName);
        assertEquals(part.getPrice(), validPrice);
        assertEquals(part.getInStock(), validInStock);
        assertEquals(part.getMin(), validMin);
        assertEquals(part.getMax(), validMax);
        assertEquals(part.getCompanyName(), validCompanyName);
    }

    @Test
    void testSetter() {
        OutsourcedPart part = null;

        part = new OutsourcedPart(0, null, 0, 0, 0, 0, null);
        part.setPartId(validId);
        part.setName(validName);
        part.setPrice(validPrice);
        part.setInStock(validInStock);
        part.setMin(validMin);
        part.setMax(validMax);
        part.setCompanyName(validCompanyName);

        assertEquals(part.getPartId(), validId);
        assertEquals(part.getName(), validName);
        assertEquals(part.getPrice(), validPrice);
        assertEquals(part.getInStock(), validInStock);
        assertEquals(part.getMin(), validMin);
        assertEquals(part.getMax(), validMax);
        assertEquals(part.getCompanyName(), validCompanyName);
    }
}