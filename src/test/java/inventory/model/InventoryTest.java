package inventory.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryTest {
    Inventory validInventory = new Inventory();
    Inventory emptyInventory = new Inventory();
    Part validPart = new InhousePart(1, "insertedPart", 1.0f, 5, 1, 10, 1);

    InventoryTest() {
        validInventory.addPart(validPart);
    }

    @Test
    void F02_P01_TC() {
        Part result;
        result = validInventory.lookupPart(null);
        assertNull(result);
    }

    @Test
    void F02_P02_TC() {
        Part result;
        result = validInventory.lookupPart("");
        assertNull(result);
    }
    @Test
    void F02_P03_TC() {
        Part result;
        result = emptyInventory.lookupPart("uninsertedPart");
        assertNull(result);
    }
    @Test
    void F02_P04_TC() {
        Part result;
        result = validInventory.lookupPart("insertedPart");
        assertEquals(result, validPart);
    }
    @Test
    void F02_P05_TC() {
        Part result;
        result = validInventory.lookupPart("1");
        assertEquals(result, validPart);
    }
    @Test
    void F02_P06_TC() {
        Part result;
        result = validInventory.lookupPart("uninsertedPart");
        assertNull(result);
    }
    @Test
    void F02_P07_TC() {
        Part result;
        result = validInventory.lookupPart("uninsertedPart");
        assertNull(result);
    }
}