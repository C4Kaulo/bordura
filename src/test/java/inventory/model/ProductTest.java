package inventory.model;

import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class ProductTest {
    private String validName = "valid";
    private double validPrice = 10.0;
    private int stoc = 50;
    private int minim = 1;
    private int maxim = 100;
    Part part = new OutsourcedPart(1,"name", 1.0, 1, 10, 5, "companyName");
    InventoryService service = new InventoryService(new InventoryRepository());
    @Test
    @DisplayName("Running ECP for stoc")
    @Tag("Stoc")
    void ECP_ValidStoc() {
        stoc = 50;
        String res = "";
        res = service.add_Product(validName, validPrice, stoc, minim, maxim, FXCollections.observableArrayList(part), res);
        assertEquals(res, "");
        assertTrue(minim <= stoc);
        assertTrue(stoc <= maxim);
    }

    @Test
    @Tag("Stoc")
    void BVA_ValidStoc()
    {
        stoc = 1;
        String res = "";
        res = service.add_Product(validName, validPrice, stoc, minim, maxim, FXCollections.observableArrayList(part), res);
        assertEquals(res, "");
        assertTrue(minim <= stoc);
        assertTrue(stoc <= maxim);
    }

    @Test
    void ECP_InvalidStoc() {
        stoc = 200;
        String res = "";
        res = service.add_Product(validName, validPrice, stoc, minim, maxim, FXCollections.observableArrayList(part), res);
        assertEquals(res, "Inventory level is higher than the maximum value. ");
        assertTrue(minim <= stoc);
        assertFalse(stoc <= maxim);
    }

    @Test
    void BVA_InvalidStoc() {
        stoc = 0;
        String res = "";
        res = service.add_Product(validName, validPrice, stoc, minim, maxim, FXCollections.observableArrayList(part), res);
        assertEquals(res, "Inventory level is lower than minimum value. ");
        assertFalse(minim <= stoc);
        assertTrue(stoc <= maxim);
    }


    @ParameterizedTest
    @ValueSource(strings = { "cali", "bali123", "dani12345", "valid" })
    @Test
    void ECP_validDenumire(String name)
    {
        String res = "";
        res = service.add_Product(validName, validPrice, stoc, minim, maxim, FXCollections.observableArrayList(part), res);
        assertEquals(res, "");
    }

    @Test
    void BVA_validDenumire()
    {
        String name = "1";
        String res = "";
        res = service.add_Product(name, validPrice, stoc, minim, maxim, FXCollections.observableArrayList(part), res);
        assertEquals(res, "");
    }
    @Disabled
    @Test
    void ECP_InvalidDenumire()
    {
        String name = "";
        String res = "";
        res = service.add_Product(name, validPrice, stoc, minim, maxim, FXCollections.observableArrayList(part), res);
    }

    @RepeatedTest(5)
    @Test
    void BVA_InvalidDenumire()
    {
        String name = "";
        String res = "";
        res = service.add_Product(name, validPrice, stoc, minim, maxim, FXCollections.observableArrayList(part), res);
        assertEquals(res, "A name has not been entered. ");
        String name2 = null;
        try
        {
            String res2 = "";
            res2 = service.add_Product(name2, validPrice, stoc, minim, maxim, FXCollections.observableArrayList(part), res);
            assertTrue(false);
        }
        catch(NullPointerException e)
        {
            assertTrue(true);
        }
    }

}