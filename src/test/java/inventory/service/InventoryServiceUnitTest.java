package inventory.service;

import inventory.model.OutsourcedPart;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;

class InventoryServiceUnitTest {
    @Mock
    InventoryRepository repository;

    @InjectMocks
    InventoryService service;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void addOutsourcePart() {
        OutsourcedPart part1 = mock(OutsourcedPart.class);
        OutsourcedPart part2 = mock(OutsourcedPart.class);

        Mockito.doNothing().when(repository).addPart(part1);
        Mockito.doNothing().when(repository).addPart(part2);
        service.addOutsourcePart(part1);
        service.addOutsourcePart(part2);

        Mockito.verify(repository, times(1)).addPart(part1);
        Mockito.verify(repository, times(1)).addPart(part2);
    }

    @Test
    void getAllPart() {
        OutsourcedPart part1 = mock(OutsourcedPart.class);
        OutsourcedPart part2 = mock(OutsourcedPart.class);
        Mockito.doNothing().when(repository).addPart(part1);
        Mockito.doNothing().when(repository).addPart(part2);
        service.addOutsourcePart(part1);
        service.addOutsourcePart(part2);
        Mockito.when(repository.getAllPart()).thenReturn(Arrays.asList(part1, part2));

        assertEquals(2, service.getAllPart().size());
        Mockito.verify(repository, times(1)).getAllPart();
    }
}